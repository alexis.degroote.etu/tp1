"use strict";

//const name = "Regina";
//const data = ['Regina', 'Napolitaine', 'Spicy'];
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var html = ""; //trie ordre alphabétique

/*data.sort((a, b) => {
    if (a.name < b.name) return -1;
    else if (a.name > b.name) return 1;
    else return 0;
});*/
//trie petit format croissant

/*data.sort((a, b) => {
    if (a.price_small < b.price_small) return -1;
    else if (a.price_small > b.price_small) return 1;
    else return 0;
});*/
//trie petit format croissant et si égalité trie grand format

data.sort(function (a, b) {
  if (a.price_small < b.price_small) return -1;else if (a.price_small > b.price_small) return 1;else if (a.price_large < b.price_large) return -1;else if (a.price_large > b.price_large) return 1;else return 0;
}); //filtrer les pizzas qui contiennent de la tomate
//const newData = data.filter(({ base }) => base == 'tomate');
//filtrer les pizzas dont les petits formats < 6 euros
//const newData = data.filter(({ price_small }) => price_small < 6);
//filtrer les pizzas dont le nom contient 2 i

var newData = data.filter(function (_ref) {
  var name = _ref.name;
  return name.split('i').length == 3;
});
html = newData.reduce(function (acc, _ref2) {
  var image = _ref2.image,
      price_small = _ref2.price_small,
      price_large = _ref2.price_large,
      name = _ref2.name;
  var url = image;
  return acc + "<article class=\"pizzaThumbnail\">\n                <a href=\"".concat(url, "\">\n                        <img src=\"").concat(url, "\"/>\n                            <section>\n                                <h4>").concat(name, "</h4>\n                                <ul>\n                                    <li>Prix petit format : ").concat(price_small.toLocaleString('fr-FR', {
    style: 'currency',
    currency: 'EUR'
  }), "</li>\n                                    <li>Prix grand format : ").concat(price_large.toLocaleString('fr-FR', {
    style: 'currency',
    currency: 'EUR'
  }), "</li>\n                                </ul>\n                            </section>\n                </a>\n            </article>");
}, "");
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map